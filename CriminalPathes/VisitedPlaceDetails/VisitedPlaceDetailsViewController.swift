//
//  VisitedPlaceDetailsViewController.swift
//  CriminalPathes
//
//  Created by Martyna Wiśnik on 30/03/2019.
//  Copyright © 2019 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit

class VisitedPlaceDetailsViewController: UITableViewController {
   
    @IBOutlet weak var placeDetailsImageView: UIImageView!
    @IBOutlet weak var placeTitleLabel: UILabel!
    @IBOutlet weak var placeLongDescriptionLabel: UILabel!
    
    var place = PlaceModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Place Details"

        
        placeDetailsImageView.image = place.image
        placeTitleLabel.text = place.title
        placeTitleLabel.titleLabelSettings()
        placeLongDescriptionLabel.text = place.story
        placeLongDescriptionLabel.numberOfLines = 0
        placeLongDescriptionLabel.adjustsFontSizeToFitWidth = false
        placeLongDescriptionLabel.regularTextSettings()
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 50

}



}
