//
//  MapViewController.swift
//  CriminalPathes
//
//  Created by Martyna Wiśnik on 30/03/2019.
//  Copyright © 2019 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class MapViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    
    @IBAction func centerButton(_ sender: UIButton) {
        centerMapOnUserButtonClicked()
    }
    var locationManager: CLLocationManager = CLLocationManager()
    let placesList = PlacesListModel()
    let radius: CLLocationDistance = 50 //meters
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        self.locationManager.startMonitoringVisits()
        locationManager.requestAlwaysAuthorization()
        locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
        AnnotationFactory.addAnnotations(mapView: self.mapView, placesList: placesList)
        mapView.userTrackingMode =  .follow
        self.view.backgroundColor = .steelBlue
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBarItem.image = UIImage(named: "planet_empty")
        self.tabBarItem.selectedImage = UIImage(named: "planet")
    }
    
    @objc func centerMapOnUserButtonClicked() {
        mapView.setUserTrackingMode(MKUserTrackingMode.follow, animated: true)
    }
    
    private func locationManager(manager: CLLocationManager,
                         didFailWithError error: NSError) {
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let lastLocation = locations.last else { return }
        isUserNearPOI(userLocation: lastLocation)
        print(lastLocation.coordinate.longitude)
        print(lastLocation.coordinate.latitude)
    }
    
    func isUserNearPOI(userLocation: CLLocation) {
        for place in placesList.places {
            let distance = userLocation.distance(from: CLLocation(latitude: place.pin?.coordinate.latitude ?? 0, longitude: place.pin?.coordinate.longitude ?? 0))
            
            if distance <= radius {
                if (UserDefaultsProvider.shared.visitedPlaces != nil && !UserDefaultsProvider.shared.visitedPlaces!.contains(place.id ?? "")) || UserDefaultsProvider.shared.visitedPlaces == nil {
                    if let visitedPlaces = UserDefaultsProvider.shared.visitedPlaces, !visitedPlaces.isEmpty {
                        UserDefaultsProvider.shared.visitedPlaces?.append(place.id ?? "")
                    } else {
                        UserDefaultsProvider.shared.visitedPlaces = [place.id ?? ""]
                    }
                    let alertController = UIAlertController(title: "Someone committed crime here!", message: "Investigate the crime!", preferredStyle: UIAlertController.Style.alert)
                    alertController.addAction(UIAlertAction(title: "Sure!", style: UIAlertAction.Style.default, handler: nil))
                    present(alertController, animated: true, completion: nil)
                }
            }
        }
        
    }
    
    func mapView(_ map: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {

        if let pin = annotation as? PinModel {



                let annotationViewReuseIdentifier = "annotationViewReuseIdentifier"
                var annotationView = map.dequeueReusableAnnotationView(withIdentifier: annotationViewReuseIdentifier)

                if annotationView == nil {
                    annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationViewReuseIdentifier)
                }

                let pinImage = UIImage(named: "unhiddenDetective")
                let size = CGSize(width: 40, height: 40)
                UIGraphicsBeginImageContext(size)
                pinImage?.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
                let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
                annotationView?.image = resizedImage
                annotationView?.annotation = annotation
                // add below line of code to enable selection on annotation view
                annotationView?.canShowCallout = true

             return annotationView
        }
        
        return annotation as? MKAnnotationView
    
        func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
    
            var markerView: MKAnnotationView?
    
            for place in placesList.places {
                if let pin = place.pin {
                    let annotationView: MKMarkerAnnotationView? = mapView.dequeueReusableAnnotationView(withIdentifier: "colored") as? MKMarkerAnnotationView
                    annotationView?.glyphImage = UIImage(named: "unhiddenDetective")
                    annotationView?.tintColor = .green
                    markerView = annotationView
    
                    mapView.addAnnotation(pin)
                }
            }
    
            for place in placesList.places {
                if UserDefaultsProvider.shared.visitedPlaces?.contains(place.id ?? "") != nil {
                    let identifier = "colored"
                    let annotationView: MKMarkerAnnotationView? = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKMarkerAnnotationView
                    annotationView?.glyphImage = UIImage(named: "hiddenDetective")
                    markerView = annotationView
                } else {
                    let identifier = "blackwhite"
                    let annotationView: MKMarkerAnnotationView? = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKMarkerAnnotationView
                    annotationView?.glyphImage = UIImage(named: "unhiddenDetective")
                    markerView = annotationView
                }
            }
      return markerView
    
       }
    
    
}
}
