//
//  UserDefaultsProvider.swift
//  CriminalPathes
//
//  Created by Martyna Wiśnik on 13/04/2019.
//  Copyright © 2019 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit

public class UserDefaultsProvider {
    
  public static let shared = UserDefaultsProvider()
    
    private init() {}
    
    let defaults = UserDefaults.standard
    let IS_PLACE_VISITED = "isVisited"
    
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }

   public var visitedPlaces: [String]? {
        get {
            if isKeyPresentInUserDefaults(key: IS_PLACE_VISITED) {
                if let decoded = defaults.data(forKey: IS_PLACE_VISITED) {
                    return NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [String]
                }}
            return nil
    }
        set {
            if let checking = newValue {
                defaults.set(NSKeyedArchiver.archivedData(withRootObject: checking), forKey: IS_PLACE_VISITED)
            } else {
                defaults.removeObject(forKey: IS_PLACE_VISITED)
            }
    }

    }
    
}

