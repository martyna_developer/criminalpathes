//
//  CoordinateModel.swift
//  CriminalPathes
//
//  Created by Martyna Wiśnik on 13/04/2019.
//  Copyright © 2019 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit
import MapKit


class PinModel: NSObject, MKAnnotation {
    
    
    var id: String?
    var title: String?
    var coordinate: CLLocationCoordinate2D
    let regionRadius: CLLocationDistance = 100
    
    init(id: String?, title: String?, coordinate: CLLocationCoordinate2D) {
      
        self.id = id
        self.title = title
        self.coordinate = coordinate
        
        super.init()
    }
}

