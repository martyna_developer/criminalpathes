//
//  PlacesListViewController.swift
//  CriminalPathes
//
//  Created by Martyna Wiśnik on 30/03/2019.
//  Copyright © 2019 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit

class PlacesListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    @IBOutlet weak var tableView: UITableView!
    
    lazy var placesList = PlacesListModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        self.title = "Places"
        tableView.separatorStyle = .none
        self.view.backgroundColor = .steelBlue
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return placesList.places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "placesListCell") as! PlacesListCell
        
        if let visitedPlaces = UserDefaultsProvider.shared.visitedPlaces, visitedPlaces.contains(placesList.places[indexPath.row].id ?? "") {
            cell.placeImageView.image = placesList.places[indexPath.row].image
            cell.placeImageView.layer.cornerRadius = cell.placeImageView.frame.height / 2
            cell.placeImageView.layer.masksToBounds = true
            cell.placeTitleLabel.text = placesList.places[indexPath.row].title
            cell.placeTitleLabel.titleLabelSettings()
            cell.shortDescriptionLabel.text = placesList.places[indexPath.row].description
            cell.shortDescriptionLabel.numberOfLines = 2
            cell.shortDescriptionLabel.undertitleLabelSettings()
            cell.isUserInteractionEnabled = true
            
        } else {
            cell.placeImageView.image = UIImage(named: "jack")
            cell.placeTitleLabel.text = "FIND OUT"
            cell.placeTitleLabel.titleLabelSettings()
            cell.shortDescriptionLabel.text = "what is hidden here"
            cell.placeImageView.layer.cornerRadius = 0
            cell.placeImageView.layer.masksToBounds = true
            cell.shortDescriptionLabel.textColor = UIColor.darkGray
            cell.shortDescriptionLabel.undertitleLabelSettings()
            cell.isUserInteractionEnabled = false
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetails" {
            if let indexPath = tableView.indexPathForSelectedRow {
                if let destinationController = segue.destination as? VisitedPlaceDetailsViewController {
                    destinationController.place = placesList.places[indexPath.row]
                }
            }
        }
    }
    
}
