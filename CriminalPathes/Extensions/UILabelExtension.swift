//
//  UILabelExtension.swift
//  CriminalPathes
//
//  Created by Martyna Wiśnik on 12/05/2019.
//  Copyright © 2019 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    
    func titleLabelSettings() {
        self.font = UIFont(name: "Gagalin-Regular", size: 25.0)
        self.textColor = UIColor.black
    }
    
    func undertitleLabelSettings() {
        self.font = UIFont(name: "Gagalin-Regular", size: 20.0)
        self.textColor = UIColor.darkGray
    }
    
    func regularTextSettings() {
        self.font = UIFont(name: "Gagalin-Regular", size: 20.0)
        self.textColor = UIColor.black
        
    }
    
    
}
