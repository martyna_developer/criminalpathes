//
//  UIColorExtension.swift
//  CriminalPathes
//
//  Created by Martyna Wiśnik on 12/05/2019.
//  Copyright © 2019 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    class var skyBlue: UIColor {
        return UIColor(red: 135.0/255.0, green: 206.0/255.0, blue: 235.0/255.0, alpha: 1.0)
    }
    
    class var floralWhite: UIColor {
        return UIColor(red: 255.0/255.0, green: 250.0/255.0, blue: 240.0/255.0, alpha: 1.0)
    }
    
    class var powderBlue: UIColor {
        return UIColor(red: 176.0/255.0, green: 224.0/255.0, blue: 230.0/255.0, alpha: 1.0)
    }
    
    class var steelBlue: UIColor {
        return UIColor(red: 70.0/255.0, green: 130.0/255.0, blue: 180.0/255.0, alpha: 1.0)
    }
    
}
