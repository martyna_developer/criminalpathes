//
//  AchievementsViewController.swift
//  CriminalPathes
//
//  Created by Martyna Wiśnik on 30/03/2019.
//  Copyright © 2019 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit

class AchievementsViewController: UIViewController {
    
    @IBOutlet weak var achievementImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .steelBlue
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setAchievementPhoto()
    }
    
    func setAchievementPhoto() {
        
        guard let numberOfVisitedPlaces = UserDefaultsProvider.shared.visitedPlaces?.count else { return }
        
        switch numberOfVisitedPlaces {
            
        case 1...4:
            print("1")
            achievementImage.image = UIImage(named: "achievementOne")
        case 5...9:
            print("5")
            achievementImage.image = UIImage(named: "achievementTwo")
        case 10...14:
            print("10")
            achievementImage.image = UIImage(named: "achievementThree")
        case 15...19:
            print("15")
            achievementImage.image = UIImage(named: "achievementFour")
        case 20:
            print("20")
            achievementImage.image = UIImage(named: "achievementFive")
        default:
            print("more and more")
            achievementImage.image = UIImage(named: "achievement")
            
        }}
    
}



