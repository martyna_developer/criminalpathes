//
//  Tabbar.swift
//  CriminalPathes
//
//  Created by Martyna Wiśnik on 11/05/2019.
//  Copyright © 2019 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit

class Tabbar: UITabBarController, UITabBarControllerDelegate {
    
    var mapViewController: MapViewController!
    var placesListViewController: PlacesListViewController!
    var achievementsTableViewController: AchievementsViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
    }
    
    
}
